import { Component } from '@angular/core';

@Component({
  selector: 'my-app',
  template: `<h1><font color = "blue">Hello {{name}}</font></h1>
  <img [src]="imadir">`,
  styles: [` 
    h1 {color:blue}
    img {width: 10%, height: 10%;}
  `],
})
export class AppComponent  { name = 'Jessica Pinzon Yam'; 
imadir = "https://i.ytimg.com/vi/jEtWy_HPpa0/hq720.jpg";

}
